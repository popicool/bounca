#!/bin/bash -e

docker-compose build
docker-compose up -d
docker-compose run bounca python3 /srv/www/bounca/manage.py migrate --noinput
echo "Visit your BounCA installation:"
